﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculatrice
{
    public partial class Calculatrice : Form
    {
        private List<Decimal> numbers = new List<Decimal>();
        private char op;
        private int calcLenght = 0;
        private Boolean rerun = false;
        public Calculatrice()
        {
            InitializeComponent();
            btn0.Click += MyButtonClick;
            btn1.Click += MyButtonClick;
            btn2.Click += MyButtonClick;
            btn3.Click += MyButtonClick;
            btn4.Click += MyButtonClick;
            btn5.Click += MyButtonClick;
            btn6.Click += MyButtonClick;
            btn7.Click += MyButtonClick;
            btn8.Click += MyButtonClick;
            btn9.Click += MyButtonClick;
            btnComma.Click += MyButtonClick;
            btnC.Click += MyButtonClick;
            btnReturn.Click += MyButtonClick;
            btnEqual.Click += MyButtonClick;
            btnPlus.Click += MyButtonClick;
            btnMinus.Click += MyButtonClick;
            btnMultiply.Click += MyButtonClick;
            btnDivide.Click += MyButtonClick;
        }

        private void MyButtonClick(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (txtCalc.Text.Length > 11 && button.Text != "<==" && button.Text != "=")
            {
                return;
            }
            switch (button.Text)
            {
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9":
                case ",":
                    if (txtCalc.Text == "0" && button.Text != "0" || rerun)
                    {
                        txtCalc.Text = "";
                        rerun = false;
                    }
                    txtCalc.Text += button.Text;
                    if (txtCalc.Text == ",")
                    {
                        txtCalc.Text = "0,";
                    }

                    if (calcLenght == 0)
                    {
                        numbers.Add(new Decimal(Convert.ToDouble(txtCalc.Text)));
                        calcLenght++;
                    }
                    else
                    {
                        if (numbers.Count < calcLenght)
                        {
                            numbers.Add(new Decimal(Convert.ToDouble(txtCalc.Text.Split('+', '-', 'x', '/')[calcLenght - 1])));
                        }
                        numbers[calcLenght-1] = Convert.ToDecimal(txtCalc.Text.Split('+','-','x','/')[calcLenght-1]);
                    }

                    break;
                case "=":
                    equals();
                    break;
                case "C":
                    txtCalc.Text = "0";
                    break;
                case "<==":
                    if (txtCalc.Text.Last() == '+' || txtCalc.Text.Last() == '-' || txtCalc.Text.Last() == 'x' || txtCalc.Text.Last() == '/')
                    {
                        calcLenght--;
                    }
                    txtCalc.Text = txtCalc.Text.Remove(txtCalc.Text.Length-1, 1);
                    if(txtCalc.Text == "")
                    {
                        txtCalc.Text = "0";
                    }
                    break;
                case "+":
                case "-":
                case "x":
                case "/":
                    if(calcLenght < 2 && txtCalc.Text.Last() != '+' && txtCalc.Text.Last() != '-' && txtCalc.Text.Last() != 'x' && txtCalc.Text.Last() != '/')
                    {
                        if (txtCalc.Text == "0")
                        {
                            txtCalc.Text = "0" + button.Text;
                            op = button.Text.ToCharArray()[0];
                            calcLenght++;
                        }
                        else
                        {
                            txtCalc.Text += button.Text;
                            op = button.Text.ToCharArray()[0];
                            calcLenght++;
                        }
                    }
                    break;
            }
        }

        private void equals()
        {
            rerun = true;
            calcLenght = 0;
            switch (op)
            {
                case '+': txtCalc.Text = Math.Round(numbers[0] + numbers[1], 2).ToString();
                    break;
                case '-': txtCalc.Text = Math.Round(numbers[0] - numbers[1], 2).ToString();
                    break;
                case 'x': txtCalc.Text = Math.Round(numbers[0] * numbers[1], 2).ToString();
                    break;
                case '/': 
                    if(numbers[0] != 0)
                    {
                        txtCalc.Text = Math.Round(numbers[0] / numbers[1],2).ToString();
                    }
                    else if (numbers[0] == 0)
                    {
                        txtCalc.Text = "Impossible";
                    }
                    break;
                default: txtCalc.Text = "0";
                    break;
            }

            numbers.Clear();
        }
    }
}
